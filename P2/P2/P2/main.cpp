/*
 * P2.cpp
 *
 * Created: 06/11/2020 21:12:43
 * Author : Tam Nguyen
 */ 

#include <avr/io.h>

#define F_CPU 16000000
#include <util/delay.h>

#define bit_toggle(Port, Bit) (Port ^= (1 << Bit))
#define bit_set(Port, Bit) (Port |= (1 << Bit))
#define bit_clear(Port, Bit) (Port &= ~(1 << Bit))
#define shift_left(Port) (Port <<= 1)
#define shift_right(Port) (Port >>= 1)

bool is_going_left = true;

void go_left() {
    // if is going to the left on D and not reached leftmost of D yet, 
    if ((PORTD < 0b10000000) && (PORTD != 0)) {
        shift_left(PORTD);
        return;
        
    // else if reached leftmost of D...
    } else if (PORTD == 0b10000000) {
        
        // "shift" the bit from D7 to B0
        shift_left(PORTD);
        bit_set(PORTB, 0);
        return;
    }
    
    // if is going to the left on B and not reached leftmost of B yet,
    if (PORTB < 0b00000010) {
        shift_left(PORTB);
    // else reached leftmost of B...
    } else {
        shift_right(PORTB);
        is_going_left = false;
    }
}

void go_right() {
    
    // if is going to the right on D and not reached rightmost of D yet,
    if (PORTD > 0b00000001) {
        shift_right(PORTD);
        return;
    // else if reached rightmost of D,
    } else if (PORTD == 0b00000001) {
        shift_left(PORTD);
        is_going_left = true;
        return;
    }
    
    // is is going to the right on B and not reached rightmost of B yet,
    if (PORTB > 0b00000001) {
        shift_right(PORTB);
    } else {
        // else "shift" the from B0 to D7
        shift_right(PORTB);   
        bit_set(PORTD, 7);
    }        
    
}

int main(void) {
    
    DDRD = 0b11111111;
    DDRB = 0b00000011;
    
    PORTD = 0x01;
    PORTB = 0x00;
    
    while (1) {
        _delay_ms(200);
        if (is_going_left)
            go_left();
        else
            go_right();
            
    }
}

