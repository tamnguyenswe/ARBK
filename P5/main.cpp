#include <iostream>
#include <thread>
#include <mutex>
#include <unistd.h>
#include "Semaphore.h"

//#include <windows.h>


// Aufgabe 1
//void thread_1() {
//    for (char c = 'a'; c <= 'z'; c++) {
//        std::cout << c;
////        usleep(50);
//    }
//    std::cout << c;
//}
//
//void thread_2() {
//    for (int i = 0; i <= 32; i++) {
//        std::cout << i;
////        usleep(50);
//    }
//    std::cout << c;
//}
//
//
//void thread_3() {
//    for (char c = 'A'; c <= 'Z'; c++) {
//        std::cout << c;
////        usleep(50);
//    }
//    std::cout << c;
//}

//std::mutex mutex;
// Aufgabe 2
//void thread_1() {
//    mutex.lock();
//    for (char c = 'a'; c <= 'z'; c++) {
//        std::cout << c;
//        usleep(50);
//    }
//    std::cout << c;
//    mutex.unlock();
//}
//
//void thread_2() {
//    mutex.lock();
//    for (int i = 0; i <= 32; i++) {
//        std::cout << i;
//        usleep(50);
//    }
//    std::cout << c;
//    mutex.unlock();
//}
//
//
//void thread_3() {
//    mutex.lock();
//    for (char c = 'A'; c <= 'Z'; c++) {
//        std::cout << c;
//        usleep(50);
//    }
//    std::cout << c;
//    mutex.unlock();
//}
Semaphore sem(1);

void thread_1() {
    sem.acquire();
    for (char c = 'a'; c <= 'z'; c++) {
        std::cout << c;
        usleep(150);
    }
    std::cout << '\n';
    sem.release();
}

void thread_2() {
    sem.acquire();
    for (int i = 0; i <= 32; i++) {
        std::cout << i;
        usleep(150);
    }
    std::cout << '\n';
    sem.release();
}


void thread_3() {
    sem.acquire();
    for (char c = 'A'; c <= 'Z'; c++) {
        std::cout << c;
        usleep(150);
    }
    std::cout << '\n';
    sem.release();
}




int main() {
    std::thread t1(thread_1);
    std::thread t2(thread_2);
    std::thread t3(thread_3);

    t3.join();
    t2.join();
    t1.join();
    return 0;
}
