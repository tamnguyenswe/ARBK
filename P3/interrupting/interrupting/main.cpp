/*
 * interrupting.cpp
 *
 * Created: 07/12/2020 19:23:29
 * Author : Tam Nguyen
 */ 

#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define ON 1
#define OFF 0

#define bit_toggle(Port, Bit) (Port ^= (1 << Bit))
#define bit_set(Port, Bit) (Port |= (1 << Bit))
#define bit_clear(Port, Bit) (Port &= ~(1 << Bit))
#define shift_left(Port) (Port <<= 1)
#define shift_right(Port) (Port >>= 1)
#define bit_read(Port, Bit) ((Port & (1<<Bit)) != 0)

/**
 * Representing a single bit of a port. Use this instead of raw bitwise operations to improve readability
 */
class PortBit {
public:
    volatile uint8_t * port; // each PORT variable represents its physical counterpart, so it must be passed by reference
    int bit;
    PortBit(volatile uint8_t * port, int bit) {
        this->port = port;
        this->bit = bit;
    }

    /**
     * Flip the state of this bit.
     */
    void toggle() {
        bit_toggle(*this->port, this->bit);
    }

    /**
     * Only use when this bit is set to input. Using on an output bit will result in an error   
     * @return True if this button is being pressed, otherwise false
     */
    bool is_pressed() {
        return bit_read(*this->port, this->bit) == OFF; // button pressed => voltage = 0
    }

    /**
     * Set this bit's value.
     * Only use when this bit is set to output. Using on an input bit will also result in an error   
     * @param The value to set to
     */
    void set(int value = 1) {
        if (value == 1) 
            bit_set(*this->port, this->bit);
        else 
            bit_clear(*this->port, this->bit);
    }
};

PortBit sw1 = PortBit(&PIND, 2);
PortBit sw2 = PortBit(&PIND, 3);
    
PortBit led_d0 = PortBit(&PORTD, 0);
PortBit led_d9 = PortBit(&PORTD, 1);
PortBit blinking_target = led_d0;

void ports_init() {
    // set port D bit 0 & 1 to out to control LEDs
    bit_set(DDRD, 0);
    bit_set(DDRD, 1);

    // set port D bit 2 & 3 to in to receive button inputs (D2 & D3 are exint ports)
    bit_clear(DDRD, 2);
    bit_clear(DDRD, 3);

    // set LEDs start state to off
    bit_clear(PORTD, 0);
    bit_clear(PORTD, 1);

    // pull up port D bit 2 & 3
    bit_set(PORTD, 2);
    bit_set(PORTD, 3);
}
bool is_blinking = true;
// External interrupt for button 1
ISR(INT0_vect) {
    while (sw1.is_pressed()); // wait until button release

    if (!is_blinking) {
        is_blinking = true;
        blinking_target = led_d0;
        return;
    }        
    
    if (blinking_target.bit == led_d0.bit) {
        is_blinking = false;   
        led_d0.set(OFF);
    } else {
        blinking_target = led_d0;
        led_d9.set(OFF);    
    }    
}

// External interrupt for button 2
ISR(INT1_vect) {
    while (sw2.is_pressed()); // wait until button release

    if (!is_blinking) {
        is_blinking = true;
        blinking_target = led_d9;
        return;
    }
    
    if (blinking_target.bit == led_d9.bit) {
        is_blinking = false;
        led_d9.set(OFF);
    } else {
        blinking_target = led_d9;
        led_d0.set(OFF);
    }
}

void external_interrupt_init() {
    cli();
    bit_set(EICRA, ISC11); // trigger interrupt with rising edge signal

    // enable external interrupt vector 0 & 1
    bit_set(EIMSK, INT0);
    bit_set(EIMSK, INT1);
    sei();
}


int main(void) {
    ports_init();
    external_interrupt_init();

    while (1) {
        if (is_blinking)
            blinking_target.toggle();
            
        _delay_ms(500);
    }
}

